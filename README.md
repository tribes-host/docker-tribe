# Docker Tribe

Docker Tribe is a Pleroma distribution with Soapbox frontend and sane default config.
This repo builds the `tribeshost/tribe` Docker image.

# License

Docker Tribe is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Docker Tribe is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Docker Tribe.  If not, see <https://www.gnu.org/licenses/>.
