FROM alpine:3.12

ARG HOME=/opt/pleroma
ARG DATA=/var/lib/pleroma

RUN echo "https://nl.alpinelinux.org/alpine/latest-stable/community" >> /etc/apk/repositories && \
  apk update && \
  apk add curl unzip exiftool imagemagick ncurses postgresql-client && \
  adduser --system --shell /bin/false --home ${HOME} pleroma && \
  mkdir -p ${DATA}/uploads && \
  mkdir -p ${DATA}/static && \
  chown -R pleroma ${DATA} && \
  mkdir -p /etc/pleroma && \
  chown -R pleroma /etc/pleroma

USER pleroma

RUN curl "https://git.pleroma.social/api/v4/projects/2/jobs/artifacts/stable/download?job=amd64-musl" -o /tmp/pleroma.zip && \
  unzip /tmp/pleroma.zip -d /tmp/ && \
  mv /tmp/release/* ${HOME} && \
  rmdir /tmp/release && \
  rm /tmp/pleroma.zip

RUN curl -L "https://gitlab.com/soapbox-pub/soapbox-fe/-/jobs/artifacts/v1.1.0/download?job=build-production" -o /tmp/soapbox-fe.zip && \
  unzip /tmp/soapbox-fe.zip -d /tmp/ && \
  mv /tmp/static/* ${DATA}/static && \
  rmdir /tmp/static && \
  rm /tmp/soapbox-fe.zip

COPY ./config.exs /etc/pleroma/config.exs
COPY ./defaults.exs /etc/pleroma/defaults.exs
COPY ./favicon.png ${DATA}/static/favicon.png
COPY ./avi.png ${DATA}/static/images/avi.png
COPY ./docker-entrypoint.sh ${HOME}

EXPOSE 4000

ENTRYPOINT ["/opt/pleroma/docker-entrypoint.sh"]
