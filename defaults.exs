import Config

domain = System.get_env("URL_DOMAIN")

admin_email = System.get_env("ADMIN_EMAIL") || "admin@#{domain}"
notify_email = System.get_env("NOTIFY_EMAIL") || "noreply@#{domain}"

config :pleroma, :instance,
  name: "Tribe",
  email: admin_email,
  notify_email: notify_email,
  description: "Just another tribe on the Fediverse",
  upload_limit: 100_000_000,
  account_activation_required: false,
  max_pinned_statuses: 5,
  user_bio_length: 1000,
  cleanup_attachments: true,
  show_reactions: false

config :pleroma, Pleroma.Upload,
  filters: [Pleroma.Upload.Filter.Dedupe, Pleroma.Upload.Filter.Exiftool]

config :pleroma, :instances_favicons, enabled: true

config :pleroma, configurable_from_database: true

# Disable non-federating chat
config :pleroma, :chat, enabled: false

config :pleroma, :welcome,
  email: [
    enabled: false,
    sender: notify_email,
    subject: "Welcome to <%= instance_name %>",
    html:
      "Hi @<%= user.nickname %>, your account is all ready to go. Hope you have fun on <%= instance_name %>!",
    text:
      "Hi @<%= user.nickname %>, your account is all ready to go. Hope you have fun on <%= instance_name %>!"
  ]

# https://www.cloudflare.com/ips/
cloudflare_ips = [
  "173.245.48.0/20",
  "103.21.244.0/22",
  "103.22.200.0/22",
  "103.31.4.0/22",
  "141.101.64.0/18",
  "108.162.192.0/18",
  "190.93.240.0/20",
  "188.114.96.0/20",
  "197.234.240.0/22",
  "198.41.128.0/17",
  "162.158.0.0/15",
  "104.16.0.0/12",
  "172.64.0.0/13",
  "131.0.72.0/22",
  "2400:cb00::/32",
  "2606:4700::/32",
  "2803:f800::/32",
  "2405:b500::/32",
  "2405:8100::/32",
  "2a06:98c0::/29",
  "2c0f:f248::/32"
]

# Be generous about what IPs we trust for X-Forwarded-For.
# Whitelist IPs from common proxy services.
config :pleroma, Pleroma.Web.Plugs.RemoteIp,
  enabled: true,
  headers: ["x-forwarded-for"],
  proxies: cloudflare_ips,
  reserved: [
    "127.0.0.0/8",
    "::1/128",
    "fc00::/7",
    "10.0.0.0/8",
    "172.16.0.0/12",
    "192.168.0.0/16"
  ]
